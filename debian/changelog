ros-rviz (1.14.25+dfsg-1) unstable; urgency=medium

  * New upstream version 1.14.25+dfsg
  * Rediff patches
  * Bump policy version (no changes)
  * Switch to QT6

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 20 Jun 2024 13:27:30 +0200

ros-rviz (1.14.20+dfsg-4) unstable; urgency=medium

  * actually upload to unstable

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 16 Jan 2024 13:55:09 +0100

ros-rviz (1.14.20+dfsg-3) experimental; urgency=medium

  * upload to unstable

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 16 Jan 2024 13:45:26 +0100

ros-rviz (1.14.20+dfsg-2) experimental; urgency=medium

  * Fix build dependency

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 15 Jan 2024 11:38:08 +0100

ros-rviz (1.14.20+dfsg-1) experimental; urgency=medium

  * New upstream version 1.14.20+dfsg
  * Bump Sonames due to ABI change
  * Update dependencies for cross builds
  * Switch to sip6

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 10 Jan 2024 23:02:15 +0100

ros-rviz (1.14.19+dfsg-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.

  [ Jochen Sprickerhof ]
  * Move librviz_default_plugin.so to lib/
  * Drop obsolete alternative build dependency
  * Bump policy version (no changes)
  * Move package description to source package

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 22 Jan 2023 19:22:49 +0100

ros-rviz (1.14.19+dfsg-3) unstable; urgency=medium

  * Add Qt dependency for ${rviz_QT_VERSION} to librviz-dev

 -- Timo Röhling <roehling@debian.org>  Fri, 04 Nov 2022 00:24:40 +0100

ros-rviz (1.14.19+dfsg-2) unstable; urgency=medium

  * Add patch for s390 ftbfs

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 07 Oct 2022 13:00:12 +0200

ros-rviz (1.14.19+dfsg-1) unstable; urgency=medium

  * New upstream version 1.14.19+dfsg

 -- Timo Röhling <roehling@debian.org>  Thu, 18 Aug 2022 19:51:56 +0200

ros-rviz (1.14.18+dfsg-1) unstable; urgency=medium

  * New upstream version 1.14.18+dfsg

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 13 Aug 2022 15:52:50 +0200

ros-rviz (1.14.15+dfsg-3) unstable; urgency=medium

  * really upload to unstable

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 04 Aug 2022 23:37:13 +0200

ros-rviz (1.14.15+dfsg-2) experimental; urgency=medium

  * upload to unstable

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 04 Aug 2022 23:29:33 +0200

ros-rviz (1.14.15+dfsg-1) experimental; urgency=medium

  * New upstream version 1.14.15+dfsg
  * Drop upstreamed patches, define RVIZ_SOVERSION
  * Bump SoName due to ABI change

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 04 Aug 2022 09:40:28 +0200

ros-rviz (1.14.14+dfsg-2) unstable; urgency=medium

  * Fix missing include
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 13 Jun 2022 19:45:12 +0200

ros-rviz (1.14.14+dfsg-1) unstable; urgency=medium

  * New upstream version 1.14.14+dfsg
  * Drop versioned dependency (version in stable)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 13 Feb 2022 09:30:46 +0100

ros-rviz (1.14.13+dfsg-3) unstable; urgency=medium

  * Fix exported location of default plugin

 -- Timo Röhling <roehling@debian.org>  Thu, 20 Jan 2022 19:15:27 +0100

ros-rviz (1.14.13+dfsg-2) unstable; urgency=medium

  [ Jochen Sprickerhof ]
  * Drop c++11 compile flags (breaks with log4cxx 0.12)

  [ Timo Röhling ]
  * Also drop CMAKE_CXX_STANDARD 14 to prevent compiler downgrade
  * Move default_plugin_location.cmake from /usr/share to /usr/lib

 -- Timo Röhling <roehling@debian.org>  Sat, 15 Jan 2022 16:30:46 +0100

ros-rviz (1.14.13+dfsg-1) unstable; urgency=medium

  * New upstream version 1.14.13+dfsg

 -- Timo Röhling <roehling@debian.org>  Fri, 17 Dec 2021 16:58:01 +0100

ros-rviz (1.14.12+dfsg-1) unstable; urgency=medium

  * New upstream version 1.14.12+dfsg

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 16 Dec 2021 11:39:59 +0100

ros-rviz (1.14.11+dfsg-1) unstable; urgency=medium

  * New upstream version 1.14.11+dfsg
  * Update d/copyright
  * Switch to dh-sequence-sip3
  * Enable tests
  * Rebase patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 22 Nov 2021 08:28:07 +0100

ros-rviz (1.14.10+dfsg-2) unstable; urgency=medium

  * Remove duplicate B-D
  * Add MA hint from MA hinter
  * upload to unstable

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 15 Oct 2021 23:31:19 +0200

ros-rviz (1.14.10+dfsg-1) experimental; urgency=medium

  * New upstream version 1.14.10+dfsg
  * Bump SONAME to 6d

 -- Timo Röhling <roehling@debian.org>  Mon, 27 Sep 2021 23:52:08 +0200

ros-rviz (1.14.4+dfsg-5) unstable; urgency=medium

  * Team upload.
  * Upload to unstable

 -- Timo Röhling <roehling@debian.org>  Wed, 22 Sep 2021 22:44:38 +0200

ros-rviz (1.14.4+dfsg-4) experimental; urgency=medium

  * Team upload.
  * Move pkg-config and CMake config files to /usr/lib/<triplet>
  * Bump Standards-Version to 4.6.0 (no changes)

 -- Timo Röhling <roehling@debian.org>  Wed, 22 Sep 2021 00:57:58 +0200

ros-rviz (1.14.4+dfsg-3) unstable; urgency=medium

  * simplify packaging

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 19 Dec 2020 18:16:07 +0100

ros-rviz (1.14.4+dfsg-2) unstable; urgency=medium

  * Add patch for Boost 1.74
  * bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 15 Dec 2020 13:59:32 +0100

ros-rviz (1.14.4+dfsg-1) unstable; urgency=medium

  * New upstream version 1.14.4+dfsg

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 18 Nov 2020 11:41:29 +0100

ros-rviz (1.14.3+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.14.3+dfsg
  * Link against OGRE 1.12

 -- Timo Röhling <timo@gaussglocke.de>  Fri, 06 Nov 2020 17:58:42 +0100

ros-rviz (1.14.1+dfsg-1) unstable; urgency=medium

  * New upstream version 1.14.1+dfsg
  * Simplify dependencies

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 07 Jul 2020 07:21:23 +0200

ros-rviz (1.14.0+dfsg-1) experimental; urgency=medium

  * New upstream version 1.14.0+dfsg
  * Remove Thomas from Uploaders, thanks for working on this
  * update copyright
  * rebase patches
  * bump debhelper versions
  * bump Soname due to ABI change

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 21 Jun 2020 14:00:39 +0200

ros-rviz (1.13.7+dfsg-2) unstable; urgency=medium

  * Support multiple Python versions
  * bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 06 Apr 2020 18:59:45 +0200

ros-rviz (1.13.7+dfsg-1) unstable; urgency=medium

  * New upstream version 1.13.7+dfsg
  * Drop Python 3 patch (upstreamed)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 21 Dec 2019 16:40:13 +0100

ros-rviz (1.13.6+dfsg-1) unstable; urgency=medium

  * New upstream version 1.13.6+dfsg
  * Rebase patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 28 Nov 2019 23:26:19 +0100

ros-rviz (1.13.5+dfsg-1) unstable; urgency=medium

  * Simplify d/rules
  * simplify d/watch
  * New upstream version 1.13.5+dfsg
  * Bump policy versions (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 27 Oct 2019 11:12:41 +0100

ros-rviz (1.13.4+dfsg-1) unstable; urgency=medium

  * New upstream version 1.13.4+dfsg
  * Update copyright

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 24 Sep 2019 07:12:25 +0200

ros-rviz (1.13.3+dfsg-2) unstable; urgency=medium

  * source only upload

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 14 Sep 2019 20:35:34 +0200

ros-rviz (1.13.3+dfsg-1) unstable; urgency=medium

  * New upstream version 1.13.3+dfsg
  * Update dependencies
  * Rebase and updates patches
  * Bump SONAME
  * switch to debhelper-compat and debhelper 12
  * Bump policy version (no changes)
  * add Salsa CI
  * Convert to Python 3 package (Closes: #938401)

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 13 Sep 2019 17:40:46 +0200

ros-rviz (1.13.1+dfsg-2) unstable; urgency=medium

  * Fix qt-bindings package name

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 09 Aug 2019 23:01:08 +0200

ros-rviz (1.13.1+dfsg-1) unstable; urgency=medium

  * Update Files-Excluded for new upstream
  * New upstream version 1.13.1+dfsg
  * Update copyright
  * Rebase patches
  * Bump Soname due to ABI change
  * Update fonts link
  * Bump policy version
  * Update dependencies

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 10 Nov 2018 09:51:19 +0100

ros-rviz (1.12.4+dfsg-3) unstable; urgency=medium

  * Update policy and debhelper versions
  * Update watch file
  * Remove parallel (Default for debhelper 10)
  * Update Vcs URLs to salsa.d.o
  * Add R³
  * http -> https
  * Update policy and debhelper versions
  * Add missing dependency and file (Closes: #896200)
  * Add patch to remove rpath
  * Strip rpath of python module
  * Remove old Lines-overrides

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 21 Apr 2018 23:00:44 +0200

ros-rviz (1.12.4+dfsg-2) unstable; urgency=medium

  * Add dependency for SVG icons

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 03 Dec 2016 10:43:21 +0100

ros-rviz (1.12.4+dfsg-1) unstable; urgency=medium

  * New upstream version 1.12.2+dfsg
  * Soname bump due to ABI change

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 05 Nov 2016 23:16:12 +0100

ros-rviz (1.12.2+dfsg-1) unstable; urgency=medium

  * New upstream version 1.12.2+dfsg
  * Update dependencies
  * rebase patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 19 Oct 2016 10:06:56 +0200

ros-rviz (1.12.1+dfsg-3) unstable; urgency=medium

  * Add missing dependency (Closes: #840794)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 15 Oct 2016 05:42:06 +0200

ros-rviz (1.12.1+dfsg-2) unstable; urgency=medium

  * Update my email address
  * Add patch for urdfdom-headers-dev 1.0

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 02 Sep 2016 10:17:34 +0200

ros-rviz (1.12.1+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 1.12.1+dfsg
  * Rebase patches
  * Fix plugin name in install rule
  * Fix dependencies (Closes: #832837)
  * Update Soname (no rdepends)
  * Add patch to fix gcc-6 FTBFS (Closes: #831135)
  * Bumped Standards-Version to 3.9.8, no changes needed.
  * Update control file to fix lintian warnings
  * Update lintian overrides

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sat, 23 Jul 2016 11:29:23 +0200

ros-rviz (1.11.10+dfsg-2) unstable; urgency=medium

  * Fix location of default_plugin

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Wed, 24 Feb 2016 17:40:49 +0100

ros-rviz (1.11.10+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #804037).

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Wed, 20 Jan 2016 22:24:14 +0000
